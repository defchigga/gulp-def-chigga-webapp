# gulp-def-chigga-webapp

> gulp web app workflow

## Installation

```shell
# global install
$ yarn global add gulp-def-chigga-webapp
$ npm install gulp-def-chigga-webapp -g

# local install
$ yarn add gulp-def-chigga-webapp -D
$ npm install gulp-def-chigga-webapp -D
```

## Config
```js
// 配置导出
module.exports = {
    mode: 'development', // 模式 'development' | 'production'(会去除css/js sourcemap文件)
    entry: 'def.config.js', // 配置入口文件
    output: 'dist', // 打包文件目录
    static: 'public', // 静态资源目录
    src: 'src', // 资源目录
    // 子资源目录基于 src
    paths: { 
        pages: '*.html', // 页面目录
        styles: 'assets/styles/**/*', // 样式目录
        scripts: 'assets/scripts/**/*', // 脚本目录
        images: 'assets/images/**/*', // 图片目录
        icons: 'assets/icons/**/*', // 图标目录
        fonts: 'assets/fonts/**/*' // 字体目录
    },
    // browserSync 开发配置
    devServer: {
        notify: false, // 是否开启提示
        port: 80, // 端口号
        open: true // 是否自动打开页面
    }
}
```
