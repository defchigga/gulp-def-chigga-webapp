#!/usr/bin/env node

// 拼接命令行 指定 gulpfile 路径 yarn gulp dev --gulpfile .\node_modules\gulp-def-chigga-webapp\lib\index.js --cwd .
process.argv.push('--gulpfile')
process.argv.push(require.resolve('..'))
process.argv.push('--cwd')
process.argv.push(process.cwd())
// 导入 gulp-cli 并执行
require('gulp/bin/gulp')
